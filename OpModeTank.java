package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by grinstkd on 11/02/2017.
 */
@TeleOp(name="Tank Drive", group="Iterative Opmode")
@Disabled
public class OpModeTank extends OpMode {
          // Declare OpMode members.
        private ElapsedTime runtime = new ElapsedTime();
        private DcMotor leftFrontMotor = null;
        private DcMotor leftBackMotor = null;
        private DcMotor rightFrontMotor = null;
        private DcMotor rightBackMotor = null;

        /*
         * Code to run ONCE when the driver hits INIT
         */
        @Override
        public void init() {
            telemetry.addData("Status", "Initialized");

            // Initialize the hardware variables. Note that the strings used here as parameters
            // to 'get' must correspond to the names assigned during the robot configuration
            // step (using the FTC Robot Controller app on the phone).
            leftFrontMotor  = hardwareMap.get(DcMotor.class, "leftfront");
            leftBackMotor  = hardwareMap.get(DcMotor.class, "leftback");
            rightFrontMotor = hardwareMap.get(DcMotor.class, "rightfront");
            rightBackMotor = hardwareMap.get(DcMotor.class, "rightback");

            // Set Power Levels to zero
            leftFrontMotor.setPower(0);
            leftBackMotor.setPower(0);
            rightFrontMotor.setPower(0);
            rightBackMotor.setPower(0);


            // Reverse the motor that runs backwards when connected directly to the battery
            leftFrontMotor.setDirection(DcMotor.Direction.FORWARD);
            leftBackMotor.setDirection(DcMotor.Direction.FORWARD);
            rightFrontMotor.setDirection(DcMotor.Direction.REVERSE);
            rightBackMotor.setDirection(DcMotor.Direction.REVERSE);

            // Tell the driver that initialization is complete.
            telemetry.addData("Status", "Initialized");
        }
        /*
         * Code to run REPEATEDLY after the driver hits INIT, but before they hit PLAY
         */
        @Override
        public void init_loop() {
        }
        /*
         * Code to run ONCE when the driver hits PLAY
         */
        @Override
        public void start() {
            runtime.reset();
        }
        /*
         * Code to run REPEATEDLY after the driver hits PLAY but before they hit STOP
         */
        @Override
        public void loop() {
            // Setup a variable for each drive wheel to save power level for telemetry
            double leftPower = -gamepad1.left_stick_y;
            double rightPower = -gamepad1.left_stick_y;

            // Tank Mode uses one stick to control each side.
            // - This requires no math, but it is hard to drive forward slowly and keep straight.
           leftPower  = -gamepad1.left_stick_y ;
           rightPower = -gamepad1.right_stick_y ;

            // Send calculated power to wheels
            leftFrontMotor.setPower(leftPower);
            leftBackMotor.setPower(leftPower);
            rightFrontMotor.setPower(rightPower);
            rightBackMotor.setPower(rightPower);
            // Show the elapsed game time and wheel power.
            //telemetry.addData("Status", "Run Time: " + runtime.toString());
            telemetry.addData("Motors", "left (%.2f), right (%.2f)", leftPower, rightPower);
            telemetry.addData( "Left/Right Stick", "LY (%.2f), RY (%.2f)", -gamepad1.left_stick_y, -gamepad1.right_stick_y);
        }
        /*
         * Code to run ONCE after the driver hits STOP
         */
        @Override
        public void stop() {
            leftFrontMotor.setPower(0);
            leftBackMotor.setPower(0);
            rightFrontMotor.setPower(0);
            rightBackMotor.setPower(0);
        }
    }
