package org.firstinspires.ftc.teamcode;

import android.hardware.Sensor;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by innobot on 11/26/2017.
 */
@TeleOp(name="InnoBot Competition", group="Iterative Opmode")

public class OpModeInnoBot extends OpMode {
    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftFrontMotor = null;
    private DcMotor leftBackMotor = null;
    private DcMotor rightFrontMotor = null;
    private DcMotor rightBackMotor = null;

    private DcMotor leftPickupMotor = null;
    private DcMotor rightPickupMotor = null;

    private DcMotor leftLiftMotor = null;

    private Servo leftGemServo0 = null;
    private Servo rightGemServo0 = null;

    private CRServo leftPlaceCRServo1 = null;
    private CRServo rightPlaceCRServo1 = null;


    private double servoPickupPosition = 0.001;
    private double servoPickupSpeed = 0.001;
    private double servoMinRange = 0.001;
    private double servoMaxRange = 1;

    private double strafeX = 0;
    private double speedSetting = 0.5;
    private double liftPower;
    private double placePower;
    private double v1;
    private double v2;
    private double v3;
    private double v4;


    /*
     * Code to run REPEATEDLY after the driver hits INIT, but before they hit PLAY
     */
    @Override
    public void init_loop() {
    }

    /*
     * Code to run ONCE when the driver hits PLAY
     */
    @Override
    public void start() {
        runtime.reset();
    }

    /*
     * Code to run REPEATEDLY after the driver hits PLAY but before they hit STOP
     */

    @Override
    public void loop() {

        // Game loop
        double r = 0;
        double robotAngle = 0;
        double rightX = 0;
        //Driving
        // Mecanum Mode

        if (gamepad1.left_trigger > 0 || gamepad1.left_bumper || gamepad1.right_trigger > 0 || gamepad1.right_bumper) {
            if (gamepad1.left_bumper) strafeX = 1;
            if (gamepad1.right_bumper) strafeX = -1;
            if (gamepad1.left_trigger > 0) strafeX = 0.35;
            if (gamepad1.right_trigger > 0) strafeX = -0.35;
        } else strafeX = 0;
        if (gamepad1.y) speedSetting = 1.0;
        if (gamepad1.x) speedSetting = 0.4;
        if (gamepad1.a) speedSetting = 0.2;
        r = Math.hypot(strafeX, gamepad1.left_stick_y);
        robotAngle = Math.atan2(gamepad1.left_stick_y, strafeX) - Math.PI / 4;
        rightX = -gamepad1.right_stick_x;
        if (speedSetting == 0.2) rightX *= 1.5;

            v1 = r * Math.cos(robotAngle) + rightX * speedSetting;
            v2 = r * Math.sin(robotAngle) - rightX * speedSetting;
            v3 = r * Math.sin(robotAngle) + rightX * speedSetting;
            v4 = r * Math.cos(robotAngle) - rightX* speedSetting;;

        // Send calculated power to wheels
        leftFrontMotor.setPower(v1);
        leftBackMotor.setPower(v3);
        rightFrontMotor.setPower(v2);
        rightBackMotor.setPower(v4);

        //Pickup
        double pickupPower1 = 0;
        double pickupPower2 = 0;
        if (gamepad2.left_stick_y != 0) {
            pickupPower1 = -gamepad2.left_stick_y;
            pickupPower2 = -gamepad2.left_stick_y;
        }
        else if (gamepad2.dpad_left){
            pickupPower1 = -1.0;
            pickupPower2 = 1.0;
        }
        else if (gamepad2.dpad_right){
            pickupPower1 = 1.0;
            pickupPower2 = -1.0;
        }
        else {
            pickupPower1 = 0;
            pickupPower2 = 0;
        }
        leftPickupMotor.setPower(pickupPower1);
        rightPickupMotor.setPower(pickupPower2);


        // Glyph Lift
        liftPower = gamepad2.right_stick_y;
        if (liftPower != 0) leftLiftMotor.setPower(liftPower);
        else leftLiftMotor.setPower(0);

        if (gamepad2.left_bumper) placePower = 1.0;
        else if (gamepad2.right_bumper) placePower = -1.0;
        else placePower = 0.0;
        leftPlaceCRServo1.setPower(placePower);
        rightPlaceCRServo1.setPower(placePower);



        //Glyph


        // Show the elapsed game time and wheel power.
        telemetry.addData("Status", "Run Time: " + runtime.toString());
        telemetry.addData("Speed Setting", "(%.2f)", speedSetting);
        telemetry.addData("1 Left/Right Stick", "LX (%.2f), LY (%.2f), RX (%.2f), RY (%.2f)", gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_stick_x, gamepad1.right_stick_y);
        telemetry.addData("2 Left/Right Stick", "LX (%.2f), LY (%.2f), RX (%.2f), RY (%.2f)", gamepad2.left_stick_x, gamepad2.left_stick_y, gamepad2.right_stick_x, gamepad2.right_stick_y);
        telemetry.addData("Place Servo Power", "(%.1f)", placePower);
        telemetry.update();
    }

    /*
     * Code to run ONCE after the driver hits STOP
     */
    @Override
    public void stop () {
        // Stop Wheel Motors
        leftFrontMotor.setPower(0);
        leftBackMotor.setPower(0);
        rightFrontMotor.setPower(0);
        rightBackMotor.setPower(0);
        // Stop Pickup Motors
        leftPickupMotor.setPower(0);
        rightPickupMotor.setPower(0);
        // Stop Transfer and Lifter Motors
        leftLiftMotor.setPower(0);
        // Stop Pickup Servo Motion
        leftGemServo0.setPosition(0.001);
        rightGemServo0.setPosition(0.001);
        // STop Eject Servo Motion
        leftPlaceCRServo1.setPower(0.0);
        rightPlaceCRServo1.setPower(0.0);
    }


    /*
     * Code to run ONCE when the driver hits INIT
     */
    @Override
    public void init () {
        telemetry.addData("Status", "Initializing");

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftFrontMotor = hardwareMap.get(DcMotor.class, "leftfront");
        leftBackMotor = hardwareMap.get(DcMotor.class, "leftback");
        rightFrontMotor = hardwareMap.get(DcMotor.class, "rightfront");
        rightBackMotor = hardwareMap.get(DcMotor.class, "rightback");

        leftPickupMotor = hardwareMap.get(DcMotor.class, "leftpickup");
        rightPickupMotor = hardwareMap.get(DcMotor.class, "rightpickup");

        leftLiftMotor = hardwareMap.get(DcMotor.class, "leftlift");

        leftGemServo0 = hardwareMap.get(Servo.class, "leftservo0");
        rightGemServo0 = hardwareMap.get(Servo.class, "rightservo0");

        leftPlaceCRServo1 = hardwareMap.get(CRServo.class, "leftcontservo1");
        rightPlaceCRServo1 = hardwareMap.get(CRServo.class, "rightcontservo1");

        final double servoPosition = 0.5;
        final double pickupPower = 0;

        // Set Power Levels to zero
        leftFrontMotor.setPower(0);
        leftBackMotor.setPower(0);
        rightFrontMotor.setPower(0);
        rightBackMotor.setPower(0);

        leftPickupMotor.setPower(0);
        rightPickupMotor.setPower(0);

        leftLiftMotor.setPower(0);

        leftPlaceCRServo1.setPower(0.0);
        rightPlaceCRServo1.setPower(0.0);


        // motor directions
        leftFrontMotor.setDirection(DcMotor.Direction.REVERSE);
        leftBackMotor.setDirection(DcMotor.Direction.REVERSE);
        rightFrontMotor.setDirection(DcMotor.Direction.FORWARD);
        rightBackMotor.setDirection(DcMotor.Direction.FORWARD);

        leftPickupMotor.setDirection(DcMotor.Direction.FORWARD);
        rightPickupMotor.setDirection(DcMotor.Direction.REVERSE);

        leftLiftMotor.setDirection(DcMotor.Direction.REVERSE);

        leftPlaceCRServo1.setDirection(CRServo.Direction.REVERSE);
        rightPlaceCRServo1.setDirection(CRServo.Direction.FORWARD);

        // Tell the driver that initialization is complete.
        telemetry.addData("Status", "Initialized");
        telemetry.update();
    }
}