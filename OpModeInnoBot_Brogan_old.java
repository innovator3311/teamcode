package org.firstinspires.ftc.teamcode;


import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.Switch;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.SwitchableLight;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by grinstkd on 11/26/2017.
 */
@TeleOp(name="InnoBot Competition - Brogan", group="Iterative Opmode")
@Disabled
public class OpModeInnoBot_Brogan_old extends OpMode {
    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftFrontMotor = null;
    private DcMotor leftBackMotor = null;
    private DcMotor rightFrontMotor = null;
    private DcMotor rightBackMotor = null;
    private DcMotor leftPickupMotor = null;
    private DcMotor rightPickupMotor = null;
    private DcMotor leftTransMotor = null;
    private DcMotor rightPlaceMotor = null;
    private Servo leftPickupServo0 = null;
    private Servo rightPickupServo0 = null;
    private CRServo topLeftEjectServo = null;
    private CRServo bottomLeftEjectServo = null;
    private CRServo topRightEjectServo = null;
    private CRServo bottomRightEjectServo = null;
    private DigitalChannel magneticSensor = null;
    NormalizedColorSensor colorSensor;
    private double servoPickupPosition = 0.001;
    private double servoPickupSpeed = 0.01;
    private double servoMinRange = 0.001;
    private double servoMaxRange = 1;

    // values is a reference to the hsvValues array.
    private float[] hsvValues = new float[3];
    private final float values[] = hsvValues;

    // bPrevState and bCurrState keep track of the previous and current state of the button
    private boolean bPrevState = false;
    private boolean bCurrState = false;

    private boolean bCurrAtMagnet = false;
    private boolean bPrevAtMagnet = false;
    /*
     * Code to run ONCE when the driver hits INIT
     */
    @Override
    public void init() {
        telemetry.addData("Status", "Initializing");

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftFrontMotor  = hardwareMap.get(DcMotor.class, "leftfront");
        leftBackMotor  = hardwareMap.get(DcMotor.class, "leftback");
        rightFrontMotor = hardwareMap.get(DcMotor.class, "rightfront");
        rightBackMotor = hardwareMap.get(DcMotor.class, "rightback");
        leftPickupMotor = hardwareMap.get(DcMotor.class, "leftpickup");
        rightPickupMotor = hardwareMap.get(DcMotor.class, "rightpickup");
        leftTransMotor = hardwareMap.get(DcMotor.class, "lefttrans");
        rightPlaceMotor = hardwareMap.get(DcMotor.class, "rightplace");
        leftPickupServo0 = hardwareMap.get(Servo.class, "leftservo0");
        rightPickupServo0 = hardwareMap.get(Servo.class, "rightservo0");
        topLeftEjectServo = hardwareMap.get(CRServo.class, "leftcontservo1");
        bottomLeftEjectServo = hardwareMap.get(CRServo.class, "leftcontservo2");
        topRightEjectServo = hardwareMap.get(CRServo.class, "rightcontservo1");
        bottomRightEjectServo = hardwareMap.get(CRServo.class, "rightcontservo2");
        colorSensor = hardwareMap.get(NormalizedColorSensor.class, "sensor_color");
        // get a reference to our digitalTouch object.
        magneticSensor = hardwareMap.get(DigitalChannel.class, "sensor_magnetic");

        // set the digital channel to input.
        magneticSensor.setMode(DigitalChannel.Mode.INPUT);

        final double servoPosition = 0.5;
        final double pickupPower = 0;

        // Set Power Levels to zero
        leftFrontMotor.setPower(0);
        leftBackMotor.setPower(0);
        rightFrontMotor.setPower(0);
        rightBackMotor.setPower(0);
        leftPickupMotor.setPower(0);
        rightPickupMotor.setPower(0);
        leftTransMotor.setPower(0);
        rightPlaceMotor.setPower(0);
        leftPickupServo0.setPosition(0.001);
        rightPickupServo0.setPosition(0.001);
        topLeftEjectServo.setPower(0.0);
        bottomLeftEjectServo.setPower(0.0);
        topRightEjectServo.setPower(0.0);
        bottomRightEjectServo.setPower(0.0);

        // Reverse the motor that runs backwards when connected directly to the battery
        leftFrontMotor.setDirection(DcMotor.Direction.REVERSE);
        leftBackMotor.setDirection(DcMotor.Direction.REVERSE);
        rightFrontMotor.setDirection(DcMotor.Direction.FORWARD);
        rightBackMotor.setDirection(DcMotor.Direction.FORWARD);
        leftPickupMotor.setDirection(DcMotor.Direction.REVERSE);
        rightPickupMotor.setDirection(DcMotor.Direction.FORWARD);
        leftTransMotor.setDirection(DcMotor.Direction.REVERSE);
        rightPlaceMotor.setDirection(DcMotor.Direction.FORWARD);

        // Tell the driver that initialization is complete.
        telemetry.addData("Status", "Initialized");
    }

    /*
     * Code to run REPEATEDLY after the driver hits INIT, but before they hit PLAY
     */
    @Override
    public void init_loop() {
    }

    /*
     * Code to run ONCE when the driver hits PLAY
     */
    @Override
    public void start() {
        final double servoPosition = 0.01;
        runtime.reset();
        leftPickupServo0.setPosition(servoPosition);
        rightPickupServo0.setPosition(servoPosition);
    }

    /*
     * Code to run REPEATEDLY after the driver hits PLAY but before they hit STOP
     */
    @Override
    public void loop() {

        double driveSlowDown = 0.5;
        double v1 = 0;
        double v2 = 0;
        double v3 = 0;
        double v4 = 0;

        // Game loop

        //Driving
        // Mecanum Mode
        double r = Math.hypot(gamepad1.left_stick_x, gamepad1.left_stick_y);
        double robotAngle = Math.atan2(gamepad1.left_stick_y, gamepad1.left_stick_x) - Math.PI / 4;
        double rightX = gamepad1.right_stick_x;
        if (gamepad1.left_bumper) {
            v1 = (r * Math.cos(robotAngle) + rightX) * driveSlowDown;
            v2 = (r * Math.sin(robotAngle) - rightX) * driveSlowDown;
            v3 = (r * Math.sin(robotAngle) + rightX) * driveSlowDown;
            v4 = (r * Math.cos(robotAngle) - rightX) * driveSlowDown;
        }
        else {
            v1 = r * Math.cos(robotAngle) + rightX;
            v2 = r * Math.sin(robotAngle) - rightX;
            v3 = r * Math.sin(robotAngle) + rightX;
            v4 = r * Math.cos(robotAngle) - rightX;
        }

        // Send calculated power to wheels
        leftFrontMotor.setPower(v1);
        leftBackMotor.setPower(v3);
        rightFrontMotor.setPower(v2);
        rightBackMotor.setPower(v4);

        //Pickup
        final double pickupPower = -1*gamepad2.left_stick_y;

        leftPickupMotor.setPower(pickupPower);
        rightPickupMotor.setPower(pickupPower);


        if (gamepad2.y){
            servoPickupPosition += servoPickupSpeed;
        }
        if(gamepad2.a){
            servoPickupPosition -= servoPickupSpeed;
        }
        servoPickupPosition  = Range.clip(servoPickupPosition, servoMinRange, servoMaxRange);
        leftPickupServo0.setPosition(servoPickupPosition);
        rightPickupServo0.setPosition(servoPickupPosition);

        //Transfer
        double transferPower = gamepad2.right_trigger;
        leftTransMotor.setPower(pickupPower);

        //Place
        if (gamepad2.right_stick_y != 0) {
            bCurrAtMagnet = magneticSensor.getState();

            double placePower = gamepad2.right_stick_y;

            if (bCurrAtMagnet == bPrevAtMagnet) {

                rightPlaceMotor.setPower(placePower);
            } else { // bCurrAtMagnet != bPrevAtMagnet
                if (bPrevAtMagnet==false){
                    rightPlaceMotor.setPower(0);
                } else {
                    rightPlaceMotor.setPower(placePower);
                }
                bPrevAtMagnet = bCurrAtMagnet;
            }
        }

        double placePower = gamepad2.right_stick_y;
        rightPlaceMotor.setPower(placePower);
        
        //Glyph

        //Servo



        // Check the status of the x button on the gamepad
        bCurrState = gamepad1.x;

        // If the button state is different than what it was, then act
        if (bCurrState != bPrevState) {
            // If the button is (now) down, then toggle the light
            if (bCurrState) {
                if (colorSensor instanceof SwitchableLight) {
                    SwitchableLight light = (SwitchableLight)colorSensor;
                    light.enableLight(!light.isLightOn());
                }
            }
        }
        bPrevState = bCurrState;

        // Read the sensor
        NormalizedRGBA colors = colorSensor.getNormalizedColors();

        /** Use telemetry to display feedback on the driver station. We show the conversion
         * of the colors to hue, saturation and value, and display the the normalized values
         * as returned from the sensor.
         * @see <a href="http://infohost.nmt.edu/tcc/help/pubs/colortheory/web/hsv.html">HSV</a>*/



        // Balance the colors. The values returned by getColors() are normalized relative to the
        // maximum possible values that the sensor can measure. For example, a sensor might in a
        // particular configuration be able to internally measure color intensity in a range of
        // [0, 10240]. In such a case, the values returned by getColors() will be divided by 10240
        // so as to return a value it the range [0,1]. However, and this is the point, even so, the
        // values we see here may not get close to 1.0 in, e.g., low light conditions where the
        // sensor measurements don't approach their maximum limit. In such situations, the *relative*
        // intensities of the colors are likely what is most interesting. Here, for example, we boost
        // the signal on the colors while maintaining their relative balance so as to give more
        // vibrant visual feedback on the robot controller visual display.
        float max = Math.max(Math.max(Math.max(colors.red, colors.green), colors.blue), colors.alpha);
        colors.red   /= max;
        colors.green /= max;
        colors.blue  /= max;
        int color = colors.toColor();
        String colorString;

        if ((Color.blue(color) > 100)&&(Color.red(color) < 100)) {
            colorString = "BLUE";
        } else if ((Color.blue(color) < 100)&&(Color.red(color) > 140)) {
            colorString = "RED";
        } else {
            colorString = "GRAY";
        }

        // Show the elapsed game time and wheel power.
        telemetry.addData("Status", "Run Time: " + runtime.toString());
        telemetry.addData("Motors", "lf (%.2f), rf (%.2f), lb (%.2f), rb (%.2f)", v1, v2, v3, v4);
        telemetry.addData( "Left/Right Stick", "LX (%.2f), LY (%.2f), RX (%.2f), RY (%.2f)", gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_stick_x, gamepad1.right_stick_y);
        telemetry.addData("Pickup Servo Possition = ", "(%.3f)",servoPickupPosition);
        telemetry.addLine("      raw color:  ")
                .addData("a", "%.3f", colors.alpha)
                .addData("r", "%.3f", colors.red)
                .addData("g", "%.3f", colors.green)
                .addData("b", "%.3f", colors.blue);

        telemetry.addLine("normalized color:  ")
                .addData("a", "%03d", Color.alpha(color))
                .addData("r", "%03d", Color.red(color))
                .addData("g", "%03d", Color.green(color))
                .addData("b", "%03d", Color.blue(color));
        telemetry.addData("COLOR", "%s", colorString);
        telemetry.update();
    }

    /*
     * Code to run ONCE after the driver hits STOP
     */
    @Override
    public void stop() {
        // Stop Motor Motion
        leftFrontMotor.setPower(0);
        leftBackMotor.setPower(0);
        rightFrontMotor.setPower(0);
        rightBackMotor.setPower(0);
        leftPickupMotor.setPower(0);
        rightPickupMotor.setPower(0);
        leftTransMotor.setPower(0);
        rightPlaceMotor.setPower(0);
        // Stop Servo Motion
        leftPickupServo0.setPosition(0.001);
        rightPickupServo0.setPosition(0.001);


    }
}
