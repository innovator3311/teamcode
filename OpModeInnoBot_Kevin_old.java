package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

/**
 * Created by grinstkd on 11/26/2017.
 */
@TeleOp(name="InnoBot Competition - Kevin", group="Iterative Opmode")
@Disabled
public class OpModeInnoBot_Kevin_old extends OpMode {
    // Declare OpMode members.
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftFrontMotor = null;
    private DcMotor leftBackMotor = null;
    private DcMotor rightFrontMotor = null;
    private DcMotor rightBackMotor = null;
    private DcMotor leftPickupMotor = null;
    private DcMotor rightPickupMotor = null;
    private DcMotor leftTransMotor = null;
    private DcMotor rightPlaceMotor = null;
    private Servo leftPickupServo0 = null;
    private Servo rightPickupServo0 = null;
    private CRServo leftContServo1 = null;
    private CRServo leftContServo2 = null;
    private CRServo rightContServo1 = null;
    private CRServo rightContServo2 = null;

    double servoPickupPosition = 0.05;
    double servoPickupSpeed = 0.005;
    double servoMinRange = 0.05;
    double servoMaxRange = 1;
    double driveSlowDown = 0.3;
    double placePower;
    double transferPower;
    double exitPower;
    double v1 = 0;
    double v2 = 0;
    double v3 = 0;
    double v4 = 0;



    /*
     * Code to run ONCE when the driver hits INIT
     */
    @Override
    public void init() {
        telemetry.addData("Status", "Initializing");

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftFrontMotor  = hardwareMap.get(DcMotor.class, "leftfront");
        leftBackMotor  = hardwareMap.get(DcMotor.class, "leftback");
        rightFrontMotor = hardwareMap.get(DcMotor.class, "rightfront");
        rightBackMotor = hardwareMap.get(DcMotor.class, "rightback");
        leftPickupMotor = hardwareMap.get(DcMotor.class, "leftpickup");
        rightPickupMotor = hardwareMap.get(DcMotor.class, "rightpickup");
        leftTransMotor = hardwareMap.get(DcMotor.class, "lefttrans");
        rightPlaceMotor = hardwareMap.get(DcMotor.class, "rightplace");
        leftPickupServo0 = hardwareMap.get(Servo.class, "leftservo0");
        rightPickupServo0 = hardwareMap.get(Servo.class, "rightservo0");
        leftContServo1 = hardwareMap.get(CRServo.class, "leftcontservo1");
        leftContServo2 = hardwareMap.get(CRServo.class, "leftcontservo2");
        rightContServo1 = hardwareMap.get(CRServo.class, "rightcontservo1");
        rightContServo2 = hardwareMap.get(CRServo.class, "rightcontservo2");

        final double servoPosition = 0.5;
        final double pickupPower = 0;

        // Set Power Levels to zero
        leftFrontMotor.setPower(0);
        leftBackMotor.setPower(0);
        rightFrontMotor.setPower(0);
        rightBackMotor.setPower(0);
        leftPickupMotor.setPower(0);
        rightPickupMotor.setPower(0);
        leftTransMotor.setPower(0);
        rightPlaceMotor.setPower(0);
        leftPickupServo0.setPosition(0.001);
        rightPickupServo0.setPosition(0.001);
        leftContServo1.setPower(0.5);
        leftContServo2.setPower(0.5);


        // Reverse the motor that runs backwards when connected directly to the battery
        leftFrontMotor.setDirection(DcMotor.Direction.REVERSE);
        leftBackMotor.setDirection(DcMotor.Direction.REVERSE);
        rightFrontMotor.setDirection(DcMotor.Direction.FORWARD);
        rightBackMotor.setDirection(DcMotor.Direction.FORWARD);
        leftPickupMotor.setDirection(DcMotor.Direction.REVERSE);
        rightPickupMotor.setDirection(DcMotor.Direction.FORWARD);
        leftTransMotor.setDirection(DcMotor.Direction.REVERSE);
        rightPlaceMotor.setDirection(DcMotor.Direction.FORWARD);

        // Tell the driver that initialization is complete.
        telemetry.addData("Status", "Initialized");
        telemetry.addData("Kevin you are a ", "dweeb");
        telemetry.update();
    }

    /*
     * Code to run REPEATEDLY after the driver hits INIT, but before they hit PLAY
     */
    @Override
    public void init_loop() {
    }

    /*
     * Code to run ONCE when the driver hits PLAY
     */
    @Override
    public void start() {
        final double servoPosition = 0.01;
        runtime.reset();
        leftPickupServo0.setPosition(servoPosition);
        rightPickupServo0.setPosition(servoPosition);
    }

    /*
     * Code to run REPEATEDLY after the driver hits PLAY but before they hit STOP
     */

    @Override
    public void loop() {

        // Game loop

        //Driving
        // Mecanum Mode
        double r = Math.hypot(gamepad1.left_stick_x, gamepad1.left_stick_y);
        double robotAngle = Math.atan2(gamepad1.left_stick_y, gamepad1.left_stick_x) - Math.PI / 4;
        double rightX = gamepad1.right_stick_x;
        if (gamepad1.left_bumper) {
            v1 = (r * Math.cos(robotAngle) + rightX) * driveSlowDown;
            v2 = (r * Math.sin(robotAngle) - rightX) * driveSlowDown;
            v3 = (r * Math.sin(robotAngle) + rightX) * driveSlowDown;
            v4 = (r * Math.cos(robotAngle) - rightX) * driveSlowDown;
        }
        else {
            v1 = r * Math.cos(robotAngle) + rightX;
            v2 = r * Math.sin(robotAngle) - rightX;
            v3 = r * Math.sin(robotAngle) + rightX;
            v4 = r * Math.cos(robotAngle) - rightX;
        }

        // Send calculated power to wheels
        leftFrontMotor.setPower(v1);
        leftBackMotor.setPower(v3);
        rightFrontMotor.setPower(v2);
        rightBackMotor.setPower(v4);

        //Pickup
        double pickupPower = -1*gamepad2.left_stick_y;

        leftPickupMotor.setPower(pickupPower);
        rightPickupMotor.setPower(pickupPower);


        if (gamepad2.y){
            servoPickupPosition += servoPickupSpeed;
        }
        if(gamepad2.a){
            servoPickupPosition -= servoPickupSpeed;
        }
        servoPickupPosition  = Range.clip(servoPickupPosition, servoMinRange, servoMaxRange);
        leftPickupServo0.setPosition(servoPickupPosition);
        rightPickupServo0.setPosition(servoPickupPosition);

        //Transfer
        transferPower = gamepad2.right_trigger;
        leftTransMotor.setPower(pickupPower);

        //Place
        placePower = gamepad2.left_stick_y;
        rightPlaceMotor.setPower(placePower);
        if (gamepad2.left_bumper)
            exitPower = 1.0;
        if(gamepad2.right_bumper)
            exitPower = -1.0;
        leftContServo1.setPower(exitPower);
        leftContServo2.setPower(exitPower);
        rightContServo1.setPower(exitPower);
        rightContServo2.setPower(exitPower);
        
        //Glyph



        // Show the elapsed game time and wheel power.
        telemetry.addData("Status", "Run Time: " + runtime.toString());
        telemetry.addData("Motors", "lf (%.2f), rf (%.2f), lb (%.2f), rb (%.2f)", v1, v2, v3, v4);
        telemetry.addData( "Left/Right Stick", "LX (%.2f), LY (%.2f), RX (%.2f), RY (%.2f)", gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_stick_x, gamepad1.right_stick_y);
        telemetry.addData("Pickup Servo Possition = ", "(%.3f)",servoPickupPosition);
        telemetry.addData("Place Servo Power", "(%.3f)",exitPower);
        telemetry.addData("Kevin drive straight","(%.3f)",gamepad2.left_stick_y);
        telemetry.update();
    }

    /*
     * Code to run ONCE after the driver hits STOP
     */
    @Override
    public void stop() {
        // Stop Motor Motion
        leftFrontMotor.setPower(0);
        leftBackMotor.setPower(0);
        rightFrontMotor.setPower(0);
        rightBackMotor.setPower(0);
        leftPickupMotor.setPower(0);
        rightPickupMotor.setPower(0);
        leftTransMotor.setPower(0);
        rightPlaceMotor.setPower(0);
        // Stop Servo Motion
        leftPickupServo0.setPosition(0.001);
        rightPickupServo0.setPosition(0.001);


    }
}
