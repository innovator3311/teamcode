package org.firstinspires.ftc.teamcode;
/**
 * Created by innovators 3311 on 1/27/2018.
 */
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name="Tedward Red Place Glyph", group="Autonomous")

public class AutoTedwardPlace extends LinearOpMode {

    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftFrontMotor = null;
    private DcMotor leftBackMotor = null;
    private DcMotor rightFrontMotor = null;
    private DcMotor rightBackMotor = null;
    private DcMotor leftPickupMotor = null;
    private DcMotor rightPickupMotor = null;
    private DcMotor leftLiftMotor = null;
    private Servo leftGemServo0 = null;
    private Servo rightGemServo0 = null;
    private CRServo leftPlaceCRServo1 = null;
    private CRServo rightPlaceCRServo1 = null;

    @Override
    public void runOpMode() {
        double delayTime = 1.0;
        double turnTime = 0.7;
        double bumpTime = 0.5;
        double driveVoltage = -0.4;
        double slowdriveVoltage = -0.3;
        double driveStop = 0;


        declareVariables();
        initializeRobot();

        //wait for Start of Autonomous
        waitForStart();

//        leftGemServo0.setPosition(1);
//        colorLeft = ColorSensor.get ();
//        if (colorLeft.R >> colorLeft.B){
//            moveRobot( driveVoltage, driveVoltage, driveVoltage, driveVoltage);
//        }
//        leftGemServo0.setPosition(0.2);


        //create Timer
        ElapsedTime eTime = new ElapsedTime();

        moveRobot( driveVoltage, driveVoltage, driveVoltage, driveVoltage);
        eTime.reset();
        while ((eTime.time() < delayTime) && (opModeIsActive())){}
        eTime.reset();
        moveRobot(-driveVoltage,-driveVoltage,driveVoltage,driveVoltage);
        while ((eTime.time() < turnTime) && (opModeIsActive())){}
        eTime.reset();
        moveRobot(-driveVoltage,-driveVoltage,-driveVoltage,-driveVoltage);
        while ((eTime.time() < delayTime) && (opModeIsActive())){}
        eTime.reset();
        leftPlaceCRServo1.setPower(-1);
        rightPlaceCRServo1.setPower(-1);
        while ((eTime.time() < bumpTime) && (opModeIsActive())){}
        eTime.reset();
        moveRobot(slowdriveVoltage,slowdriveVoltage,slowdriveVoltage,slowdriveVoltage);
        while ((eTime.time() < bumpTime) && (opModeIsActive())){}
        eTime.reset();
        moveRobot(-slowdriveVoltage,-slowdriveVoltage,-slowdriveVoltage,-slowdriveVoltage);
        while ((eTime.time() < bumpTime) && (opModeIsActive())){}
        eTime.reset();
        moveRobot(slowdriveVoltage,slowdriveVoltage,slowdriveVoltage,slowdriveVoltage);
        while ((eTime.time() < 0.2) && (opModeIsActive())){}
        moveRobot(driveStop,driveStop,driveStop,driveStop);
        leftPlaceCRServo1.setPower(0);
        rightPlaceCRServo1.setPower(0);

    }
    public void declareVariables () {

        double strafeX = 0;
        double speedSetting = 0.5;
        double liftPower;
        double placePower;
        double v1;
        double v2;
        double v3;
        double v4;
    }

    //Initialize Robot
    public void initializeRobot(){
        telemetry.addData("Status", "Initializing");

        // Initialize the hardware variables. Note that the strings used here as parameters
        // to 'get' must correspond to the names assigned during the robot configuration
        // step (using the FTC Robot Controller app on the phone).
        leftFrontMotor = hardwareMap.get(DcMotor.class, "leftfront");
        leftBackMotor = hardwareMap.get(DcMotor.class, "leftback");
        rightFrontMotor = hardwareMap.get(DcMotor.class, "rightfront");
        rightBackMotor = hardwareMap.get(DcMotor.class, "rightback");

        leftPickupMotor = hardwareMap.get(DcMotor.class, "leftpickup");
        rightPickupMotor = hardwareMap.get(DcMotor.class, "rightpickup");


        leftLiftMotor = hardwareMap.get(DcMotor.class, "leftlift");

        leftGemServo0 = hardwareMap.get(Servo.class, "leftservo0");
        rightGemServo0 = hardwareMap.get(Servo.class, "rightservo0");

        leftPlaceCRServo1 = hardwareMap.get(CRServo.class, "leftcontservo1");
        rightPlaceCRServo1 = hardwareMap.get(CRServo.class, "rightcontservo1");

        final double servoPosition = 0.5;
        final double pickupPower = 0;

        // Set Power Levels to zero
        leftFrontMotor.setPower(0);
        leftBackMotor.setPower(0);
        rightFrontMotor.setPower(0);
        rightBackMotor.setPower(0);

        leftPickupMotor.setPower(0);
        rightPickupMotor.setPower(0);

        leftLiftMotor.setPower(0);

        leftPlaceCRServo1.setPower(0.0);
        rightPlaceCRServo1.setPower(0.0);


        // motor directions
        leftFrontMotor.setDirection(DcMotor.Direction.REVERSE);
        leftBackMotor.setDirection(DcMotor.Direction.REVERSE);
        rightFrontMotor.setDirection(DcMotor.Direction.FORWARD);
        rightBackMotor.setDirection(DcMotor.Direction.FORWARD);

        leftPickupMotor.setDirection(DcMotor.Direction.FORWARD);
        rightPickupMotor.setDirection(DcMotor.Direction.REVERSE);

        leftLiftMotor.setDirection(DcMotor.Direction.REVERSE);

        leftPlaceCRServo1.setDirection(CRServo.Direction.REVERSE);
        rightPlaceCRServo1.setDirection(CRServo.Direction.FORWARD);

        // Tell the driver that initialization is complete.
        telemetry.addData("Status", "Initialized");
    }
    public void moveRobot(double p1, double p2, double p3, double p4){
        leftFrontMotor.setPower(p1);
        leftBackMotor.setPower(p2);
        rightFrontMotor.setPower(p3);
        rightBackMotor.setPower(p4);


    }
}
