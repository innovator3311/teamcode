package org.firstinspires.ftc.teamcode;

import android.graphics.Color;
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import java.util.Objects;

public class Relic_Jen_InnoBot {

    ////////////////////////////////////////////////////////////
    // MOTOR DECLARATIONS
    // WHEELS
    public DcMotor  wheelMotor_leftFront = null;
    public DcMotor  wheelMotor_leftBack = null;
    public DcMotor  wheelMotor_rightFront = null;
    public DcMotor  wheelMotor_rightBack = null;
    // PICKUP
    public DcMotor  pickupMotor_left = null;
    public DcMotor  pickupMotor_right = null;
    // TRANSFER
    public DcMotor  transferMotor_solo = null;
    // LIFTER
    public DcMotor  lifterMotor_solo = null;

    ////////////////////////////////////////////////////////////
    // SERVO DECLARATIONS
    // PICKUP ANGLE CONTROL SERVO
    public Servo    pickupAngleServo_left = null;
    public Servo    pickupAngleServo_right = null;

    ////////////////////////////////////////////////////////////
    // CONTINUOUS SERVO DECLARATIONS
    // PLACER CONTINUOUS SERVOS
    public CRServo  placerCServo_leftTop = null;
    public CRServo  placerCServo_leftBot = null;
    public CRServo  placerCServo_rightTop = null;
    public CRServo  placerCServo_rightBot = null;

    ////////////////////////////////////////////////////////////
    // SENSOR DECLARATIONS
    // COLOR SENSORS
    public NormalizedColorSensor colorSensor_left = null;
    public NormalizedColorSensor colorSensor_right = null;
    // GYRO SENSOR
    public BNO055IMU gyroSensor_IMU = null;


    public double pickupAngleServoPositionMinValue = 0.0;
    public double pickupAngleServoPositionMaxValue = 1.0;

    // Initialize the hardware variables with names assigned
    // in Configuration on the Robot Controller app.
    public void init(HardwareMap hardwareMap){

        ////////////////////////////////////////////////////////////
        // WHEEL MOTORS
        wheelMotor_leftFront  = hardwareMap.get(DcMotor.class, "leftfront");
        wheelMotor_leftFront.setDirection(DcMotor.Direction.REVERSE);
        wheelMotor_leftFront.setPower(0);
        wheelMotor_leftBack  = hardwareMap.get(DcMotor.class, "leftback");
        wheelMotor_leftBack.setDirection(DcMotor.Direction.REVERSE);
        wheelMotor_leftBack.setPower(0);
        wheelMotor_rightFront = hardwareMap.get(DcMotor.class, "rightfront");
        wheelMotor_rightFront.setDirection(DcMotor.Direction.FORWARD);
        wheelMotor_rightFront.setPower(0);
        wheelMotor_rightBack = hardwareMap.get(DcMotor.class, "rightback");
        wheelMotor_rightBack.setDirection(DcMotor.Direction.FORWARD);
        wheelMotor_rightBack.setPower(0);
        // PICKUP MOTORS
        pickupMotor_left = hardwareMap.get(DcMotor.class, "leftpickup");
        pickupMotor_left.setDirection(DcMotor.Direction.REVERSE);
        pickupMotor_left.setPower(0);
        pickupMotor_right = hardwareMap.get(DcMotor.class, "rightpickup");
        pickupMotor_right.setDirection(DcMotor.Direction.FORWARD);
        pickupMotor_right.setPower(0);
        // TRANSFER MOTOR
        transferMotor_solo = hardwareMap.get(DcMotor.class, "lefttrans");
        transferMotor_solo.setDirection(DcMotor.Direction.REVERSE);
        transferMotor_solo.setPower(0);
        // LIFTER MOTOR
        lifterMotor_solo = hardwareMap.get(DcMotor.class, "rightplace");
        lifterMotor_solo.setDirection(DcMotor.Direction.FORWARD);
        lifterMotor_solo.setPower(0);

        ////////////////////////////////////////////////////////////
        // SERVO INITIALIZATIONS
        // PICKUP ANGLE CONTROLLER
        pickupAngleServo_left = hardwareMap.get(Servo.class, "leftservo0");
        pickupAngleServo_left.setPosition(0.0);
        pickupAngleServo_right = hardwareMap.get(Servo.class, "rightservo0");
        pickupAngleServo_right.setPosition(0.0);

        ////////////////////////////////////////////////////////////
        // CONTINUOUS SERVO INITIALIZATIONS
        // PLACER SERVOS
        placerCServo_leftTop = hardwareMap.get(CRServo.class, "leftcontservo1");
        placerCServo_leftTop.setPower(0.0);
        placerCServo_leftBot = hardwareMap.get(CRServo.class, "leftcontservo2");
        placerCServo_leftBot.setPower(0.0);
        placerCServo_rightTop = hardwareMap.get(CRServo.class, "rightcontservo1");
        placerCServo_rightTop.setPower(0.0);
        placerCServo_rightBot = hardwareMap.get(CRServo.class, "rightcontservo2");
        placerCServo_rightBot.setPower(0.0);

        ////////////////////////////////////////////////////////////
        // SENSOR INITIALIZATIONS
        // COLOR SENSORS
        colorSensor_left = hardwareMap.get(NormalizedColorSensor.class, "sensor_color_left");
        colorSensor_right = hardwareMap.get(NormalizedColorSensor.class, "sensor_color_right");
        // GYRO SENSOR
        gyroSensor_IMU = hardwareMap.get(BNO055IMU.class, "sensor_GyroIMU");
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled = true;
        parameters.loggingTag = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        gyroSensor_IMU.initialize(parameters);


    }

    ////////////////////////////////////////////////////////////
    // PUBLICLY ACCESSIBLE METHODS TO CONTROL BASIC ROBOT FUNCTIONALITY
    public void startDriving(double power) {
        wheelMotor_leftFront.setPower(power);
        wheelMotor_leftBack.setPower(power);
        wheelMotor_rightFront.setPower(power);
        wheelMotor_rightBack.setPower(power);
    }
    public void stopDriving() { stopWheels(); }

    public void startStrafing(double power) {
        wheelMotor_leftFront.setPower(-power);
        wheelMotor_leftBack.setPower(power);
        wheelMotor_rightFront.setPower(power);
        wheelMotor_rightBack.setPower(-power);
    }
    public void stopStrafing() { stopWheels(); }

    public void stopWheels() {
        wheelMotor_leftFront.setPower(0);
        wheelMotor_leftBack.setPower(0);
        wheelMotor_rightFront.setPower(0);
        wheelMotor_rightBack.setPower(0);
    }

    public void startSpinningCW(double power) {
        wheelMotor_leftFront.setPower(power);
        wheelMotor_leftBack.setPower(power);
        wheelMotor_rightFront.setPower(-power);
        wheelMotor_rightBack.setPower(-power);
    }
    public void startSpinningCCW(double power) {
        startSpinningCW(-power);
    }
    public void stopSpinning() { stopDriving(); }

    public void startLifter(double power) { lifterMotor_solo.setPower(power); }
    public void stopLifter() { lifterMotor_solo.setPower(0); }

    public void startTransfer(double power) { transferMotor_solo.setPower(power); }
    public void stopTransfer() { transferMotor_solo.setPower(0); }

    public void startPickup(double power) { pickupMotor_right.setPower(power); pickupMotor_left.setPower(power); }
    public void stopPickup() { pickupMotor_right.setPower(0); pickupMotor_left.setPower(0); }

    public void stopAllMotors() {
        stopWheels();
        stopPickup();
        stopLifter();
        stopTransfer();
    }

    public float getCurrentHeading () {
        return gyroSensor_IMU.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES).firstAngle;
    }

    public Boolean sensorSeesColor(String sensorSide, String color) {
        if (colorStringIsEqual(getColorString(sensorSide), color)) {
            return true;
        } else {
            return false;
        }
    }

    private Boolean colorStringIsEqual(String color1, String color2) {
        return (Objects.equals(color1, color2));
    }

    public String getColorString(String sensorSide ) {
        String sPerceivedColor = "";
        NormalizedRGBA colorSensorData;

        if (Objects.equals(sensorSide, "LEFT")) {
            colorSensorData = colorSensor_left.getNormalizedColors();
        } else {
            colorSensorData = colorSensor_right.getNormalizedColors();
        }
        float max = Math.max(Math.max(Math.max(colorSensorData.red, colorSensorData.green), colorSensorData.blue), colorSensorData.alpha);
        colorSensorData.red /= max;
        colorSensorData.green /= max;
        colorSensorData.blue /= max;
        int color = colorSensorData.toColor();

        if ((Color.blue(color) > 70) && (Color.red(color) < 100)) {
            sPerceivedColor = "BLUE";
        } else if ((Color.blue(color) < 70) && (Color.red(color) > 120)) {
            sPerceivedColor = "RED";
        } else {
            sPerceivedColor = "GRAY";
        }
        return sPerceivedColor;
    }
}