package org.firstinspires.ftc.teamcode;

import android.graphics.Color;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.util.ElapsedTime;
import org.firstinspires.ftc.robotcore.external.Func;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

import java.util.Locale;
import java.util.Objects;

import static com.qualcomm.robotcore.util.ElapsedTime.Resolution.SECONDS;

@Autonomous(name="AutoTesting", group="Autonomous")
@Disabled
public class Relic_Jen_Master_Autonomous extends LinearOpMode {
    private Relic_Jen_InnoBot innoBot = new Relic_Jen_InnoBot();
    private ElapsedTime     RUNTIMEINSECONDS = new ElapsedTime(SECONDS);
    private String          TEAMCOLOR;
    private final double    HEADING_THRESHOLD = 2.0;
    private final double    DRIVE_SPEED_SLOW = 0.25;
    private final double    DRIVE_SPEED_FAST = 1.0;

    Orientation             gyroSensorData_angle;
    Acceleration            gyroSensorData_gravity;

    @Override
    public void runOpMode() throws InterruptedException {

        telemetry.addData("JStatus", "start robot init method: innoBot.init()");
        telemetry.update();
        innoBot.init(hardwareMap);
        TEAMCOLOR = innoBot.getColorString("LEFT");
        telemetry.addData("JStatus", "detected color %s on LEFT.", TEAMCOLOR);
        telemetry.addData("JStatus", "done w/ robot init method: innoBot.init()");
        telemetry.addData("JStatus", "Waiting for Start Button");
        telemetry.update();

        waitForStart();

        // Set up our telemetry dashboard
        //composeTelemetry();

        telemetry.setAutoClear(false);
        if (true) {
            telemetry.addData("JStatus", "Driving forward looking for RED.");
            telemetry.update();
            driveForwardUntilEitherSensorIsOverColor(DRIVE_SPEED_SLOW, "RED", 3.0);
            pauseExecutionForSeconds(2.0);
            telemetry.addData("JStatus", "Driving backward looking for BLUE.");
            telemetry.update();
            driveBackwardUntilEitherSensorIsOverColor(DRIVE_SPEED_SLOW, "BLUE", 3.0);
        }

        if (true) {
            telemetry.addData("JStatus", "Running pickup for 1.0 seconds.");
            telemetry.update();
            runPickupAtPowerForSeconds(1.0, 1.0);
        }

        if (true) {
            telemetry.addData("JStatus", "Running transfer for 1.0 seconds.");
            telemetry.update();
            runTransferAtPowerForSeconds(1.0, 1.0);
        }

        if (true) { // TEST TURN TO ABSOLUTE HEADING
            telemetry.addData("JStatus", "Spinning to face 45 degrees.");
            telemetry.update();
            spinToFaceHeading(45, 5.0);
            pauseExecutionForSeconds(2.0);
        }

        if (true) { // TEST STRAFING FOR 2 SECONDS
            telemetry.addData("JStatus", "Strafing LEFT for 1 second.");
            telemetry.update();
            strafeLeftAtPowerForSeconds(DRIVE_SPEED_FAST, 1.0);
            pauseExecutionForSeconds(2.0);
            telemetry.addData("JStatus", "Strafing RIGHT for 1 second.");
            telemetry.update();
            strafeRightAtPowerForSeconds(DRIVE_SPEED_FAST, 1.0);
        }

        if (true) { // TEST DRIVING FOR 2 SECONDS
            telemetry.addData("JStatus", "Driving FORWARD for 1 second.");
            telemetry.update();
            driveForwardAtPowerForSeconds(DRIVE_SPEED_SLOW, 1.0);
            pauseExecutionForSeconds(2.0);
            telemetry.addData("JStatus", "Driving BACKWARD for 1 second.");
            telemetry.update();
            driveBackwardAtPowerForSeconds(DRIVE_SPEED_SLOW, 1.0);
        }

        pauseExecutionForSeconds(10.0);
        innoBot.stopAllMotors();
    }

    private void driveForwardUntilEitherSensorIsOverColor(double power, String color, double timeoutS) throws InterruptedException {
        Boolean bFoundColor = false;

        RUNTIMEINSECONDS.reset();
        while ((bFoundColor == false) && opModeIsActive() && (RUNTIMEINSECONDS.seconds() < timeoutS)) {
            innoBot.startDriving(power);
            if ((innoBot.sensorSeesColor("LEFT", color) == true) ||
                    (innoBot.sensorSeesColor("RIGHT", color) == true)) {
                bFoundColor = true;
            }
            //composeTelemetry();
        }
        innoBot.stopDriving();
    }

    private void driveForwardUntilEitherSensorIsOverTeamColor(double power, double timeoutS) throws InterruptedException {
        driveForwardUntilEitherSensorIsOverColor(power, TEAMCOLOR, timeoutS);
    }

    private void driveBackwardUntilEitherSensorIsOverColor(double power, String color, double timeoutS) throws InterruptedException {
        driveForwardUntilEitherSensorIsOverColor(-power, color, timeoutS);
    }

    private Boolean sensorSeesTeamColor(String sensorSide) { return stringIsTeamColor(innoBot.getColorString(sensorSide)); }
    private Boolean stringIsTeamColor(String color) { return isColorStringEqual(TEAMCOLOR, color); }
    private Boolean isColorStringEqual(String color1, String color2) { return Objects.equals(color1, color2); }

    /////////////////////////////////////////////////////////////////////////////////////////
    // TURNING METHODS
    /////////////////////////////////////////////////////////////////////////////////////////
    private void spinToFaceHeading(double targetHeading, double timeoutS) throws InterruptedException {
        if (opModeIsActive()) {
            float currentHeading;
            double degreesToTarget;
            double pidPower = 0;

            degreesToTarget = getDegreesToTarget(targetHeading);
            RUNTIMEINSECONDS.reset();
            while ((Math.abs(degreesToTarget) > HEADING_THRESHOLD) &&
                    (opModeIsActive()) &&
                    (RUNTIMEINSECONDS.seconds() < timeoutS)) {

                degreesToTarget = getDegreesToTarget(targetHeading);
                if (Math.abs(degreesToTarget) < 15) {
                    pidPower = (degreesToTarget / 180) + (degreesToTarget > 0 ? 0.2 : -0.2);;
                } else {
                    if (degreesToTarget > 0) {
                        pidPower = 0.3;
                    } else {
                        pidPower = -0.3;
                    }
                }

                innoBot.startSpinningCW(pidPower);

                currentHeading = innoBot.getCurrentHeading();

                telemetry.addData("targetHeading", targetHeading);
                telemetry.addData("currentHeading", currentHeading);
                telemetry.addData("degreesToTarget", degreesToTarget);
                telemetry.addData("pidPower", pidPower);
                telemetry.update();
            }
            innoBot.stopWheels();
        }
    }

    private double getDegreesToTarget(double targetAngle) throws InterruptedException {
        double robotError = targetAngle - innoBot.getCurrentHeading();
        while (opModeIsActive() && (robotError > 180)) robotError -= 360;
        while (opModeIsActive() && (robotError <= -180)) robotError += 360;
        return robotError;
    }



    private void pauseExecutionForSeconds(double durationS) throws InterruptedException {
        RUNTIMEINSECONDS.reset( );
        telemetry.setAutoClear(false);
        telemetry.addData("JStatus2", "pausing execution for %.1f seconds...", durationS);
        telemetry.update();
        while (opModeIsActive() &&
                (RUNTIMEINSECONDS.seconds() < durationS)) {
            idle();
        }
    }

    private void driveForwardAtPowerForSeconds(double power, double durationS) throws InterruptedException {
        innoBot.startDriving(power);
        RUNTIMEINSECONDS.reset();
        while (opModeIsActive() && (RUNTIMEINSECONDS.seconds() < durationS)) {
            idle();
        }
        innoBot.stopDriving();
    }
    private void driveBackwardAtPowerForSeconds(double power, double durationS) throws InterruptedException {
        driveForwardAtPowerForSeconds(-power, durationS);
    }

    private void strafeLeftAtPowerForSeconds(double power, double durationS) throws InterruptedException {
        innoBot.startStrafing(power);
        RUNTIMEINSECONDS.reset();
        while (opModeIsActive() && (RUNTIMEINSECONDS.seconds() < durationS)) {
            idle();
        }
        innoBot.stopStrafing();
    }
    private void strafeRightAtPowerForSeconds(double power, double durationS) throws InterruptedException {
        strafeLeftAtPowerForSeconds(-power, durationS);
    }

    private void runPickupAtPowerForSeconds(double power, double durationS) throws InterruptedException {
        innoBot.startPickup(power);
        RUNTIMEINSECONDS.reset();
        while (opModeIsActive() && (RUNTIMEINSECONDS.seconds() < durationS)) {
            idle();
        }
        innoBot.stopPickup();
    }

    private void runTransferAtPowerForSeconds(double power, double durationS) throws InterruptedException {
        innoBot.startTransfer(power);
        RUNTIMEINSECONDS.reset();
        while (opModeIsActive() && (RUNTIMEINSECONDS.seconds() < durationS)) {
            idle();
        }
        innoBot.stopTransfer();
    }

    //----------------------------------------------------------------------------------------------
    // Telemetry Configuration
    //----------------------------------------------------------------------------------------------
    private void composeTelemetry() {
        telemetry.addAction(new Runnable() { @Override public void run()
        {
            gyroSensorData_angle = innoBot.gyroSensor_IMU.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
            gyroSensorData_gravity = innoBot.gyroSensor_IMU.getGravity();

        }
        });

        telemetry.addData("Run Time: %s", RUNTIMEINSECONDS.toString());
        telemetry.addLine("Gyro:  ")
                .addData("heading", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(gyroSensorData_angle.angleUnit, gyroSensorData_angle.firstAngle);
                    }
                })
                .addData("roll", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(gyroSensorData_angle.angleUnit, gyroSensorData_angle.secondAngle);
                    }
                })
                .addData("pitch", new Func<String>() {
                    @Override public String value() {
                        return formatAngle(gyroSensorData_angle.angleUnit, gyroSensorData_angle.thirdAngle);
                    }
                });

        NormalizedRGBA colorSensorData;
        float max;
        int color;
        colorSensorData = innoBot.colorSensor_left.getNormalizedColors();
        max = Math.max(Math.max(Math.max(colorSensorData.red, colorSensorData.green), colorSensorData.blue), colorSensorData.alpha);
        colorSensorData.red /= max; colorSensorData.green /= max; colorSensorData.blue /= max;
        color = colorSensorData.toColor();


        telemetry.addLine("LColor: ")
                .addData("red", "%03d", Color.red(color))
                .addData("green", "%03d", Color.green(color))
                .addData("blue", "%03d", Color.blue(color));

        colorSensorData = innoBot.colorSensor_right.getNormalizedColors();
        max = Math.max(Math.max(Math.max(colorSensorData.red, colorSensorData.green), colorSensorData.blue), colorSensorData.alpha);
        colorSensorData.red /= max; colorSensorData.green /= max; colorSensorData.blue /= max;
        color = colorSensorData.toColor();

        telemetry.addLine("RColor: ")
                .addData("red", "%03d", Color.red(color))
                .addData("green", "%03d", Color.green(color))
                .addData("blue", "%03d", Color.blue(color));
    }

    //----------------------------------------------------------------------------------------------
    // Formatting
    //----------------------------------------------------------------------------------------------

    private String formatAngle(AngleUnit angleUnit, double angle) {
        return formatDegrees(AngleUnit.DEGREES.fromUnit(angleUnit, angle));
    }

    private String formatDegrees(double degrees){
        return String.format(Locale.getDefault(), "%.1f", AngleUnit.DEGREES.normalize(degrees));
    }
}
