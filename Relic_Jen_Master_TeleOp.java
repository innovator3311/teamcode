package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

/**
 * This file provides basic TeleOp driving for the Innovators 3311 robot.
 * The code is structured as an Iterative OpMode
 *
 * This OpMode uses the common InnoBot hardware class to define the devices on the robot.
 */

@TeleOp(name="TeleOp Master", group="InnoBot")
@Disabled
public class Relic_Jen_Master_TeleOp extends OpMode{

    Relic_Jen_InnoBot innoBot = new Relic_Jen_InnoBot(); // use the class created to define an InnoBot's hardware
    private ElapsedTime runtime = new ElapsedTime();
    private double pickupAngleServoPosition = 0.000;

    /* Code to run REPEATEDLY after the driver hits PLAY but before they hit STOP */
    @Override
    public void loop() {

        double driveSlowDown = 0.5;

        double pickupAngleServoPositionChangeRate = 0.001;

        // PICKUP ANGLE
        // Change pickup wheel angle by changing the position of the pickupAngleServos
        // gamepad2.y to go up
        // gamepad2.a to go down
        if (gamepad2.y) {
            pickupAngleServoPosition += pickupAngleServoPositionChangeRate;
        }
        if (gamepad2.a) {
            pickupAngleServoPosition -= pickupAngleServoPositionChangeRate;
        }
        pickupAngleServoPosition = Range.clip(pickupAngleServoPosition, innoBot.pickupAngleServoPositionMinValue, innoBot.pickupAngleServoPositionMaxValue);
        innoBot.pickupAngleServo_left.setPosition(pickupAngleServoPosition);
        innoBot.pickupAngleServo_right.setPosition(pickupAngleServoPosition);

        //PICKUP AND TRANSFER MOTOR POWER
        // gamepad2.left_stick_y
        //   up/forward to go forward
        //   down/backward to go backward
        double pickupMotorPower = 0;
        double transferMotorPower;
        if (gamepad2.left_stick_y != 0) {
            pickupMotorPower = gamepad2.left_stick_y;
            transferMotorPower = gamepad2.left_stick_y;
        } else {
            pickupMotorPower = 0;
            transferMotorPower = 0;
        }
        innoBot.pickupMotor_left.setPower(pickupMotorPower);
        innoBot.pickupMotor_right.setPower(pickupMotorPower);
        innoBot.transferMotor_solo.setPower(transferMotorPower);

        //LIFTER MOTOR
        // gamepad2.right_stick_y
        //   up/forward to go up
        //   down/backward to go down
        double lifterMotorPower = 0;
        if (gamepad2.right_stick_y != 0) {
            lifterMotorPower = gamepad2.right_stick_y;
        } else {
            lifterMotorPower = 0;
        }
        innoBot.lifterMotor_solo.setPower(lifterMotorPower);

        //PLACER SERVOS - BOTTOM
        // gamepad2.left_bumper
        //   press left_bumper to push glyph forward
        // gamepad2.left_trigger
        //   press left trigger to push glyph backward
        double placerCServoPower_bot;
        if (gamepad2.left_bumper) {
            placerCServoPower_bot = 1.0;
        } else if (gamepad2.left_trigger > 0) {
            placerCServoPower_bot = -1.0;
        } else {
            placerCServoPower_bot = 0.0;
        }
        innoBot.placerCServo_leftBot.setPower(-placerCServoPower_bot);
        innoBot.placerCServo_rightBot.setPower(placerCServoPower_bot);

        //PLACER SERVOS - TOP
        // gamepad2.right_bumper
        //   press right_bumper to push glyph forward
        // gamepad2.right_trigger
        //   press right_trigger to push glyph backward
        double placerCServoPower_top;
        if (gamepad2.right_bumper) {
            placerCServoPower_top = 1.0;
        } else if (gamepad2.right_trigger > 0) {
            placerCServoPower_top = -1.0;
        } else {
            placerCServoPower_top = 0.0;
        }
        innoBot.placerCServo_leftTop.setPower(-placerCServoPower_top);
        innoBot.placerCServo_rightTop.setPower(placerCServoPower_top);


        // WHEEL MOTORS
        // Mechanum wheels
        double v1 = 0;
        double v2 = 0;
        double v3 = 0;
        double v4 = 0;

        double r = Math.hypot(gamepad1.left_stick_x, gamepad1.left_stick_y);
        double robotAngle = Math.atan2(gamepad1.left_stick_y, -gamepad1.left_stick_x) - Math.PI / 4;
        double rightX = gamepad1.right_stick_x;
        if (gamepad1.left_bumper) {
            v1 = (r * Math.cos(robotAngle) - rightX) * driveSlowDown;
            v2 = (r * Math.sin(robotAngle) + rightX) * driveSlowDown;
            v3 = (r * Math.sin(robotAngle) - rightX) * driveSlowDown;
            v4 = (r * Math.cos(robotAngle) + rightX) * driveSlowDown;
        } else {
            v1 = r * Math.cos(robotAngle) - rightX;
            v2 = r * Math.sin(robotAngle) + rightX;
            v3 = r * Math.sin(robotAngle) - rightX;
            v4 = r * Math.cos(robotAngle) + rightX;
        }

        // Send calculated power to wheels
        innoBot.wheelMotor_leftFront.setPower(v1);
        innoBot.wheelMotor_leftBack.setPower(v3);
        innoBot.wheelMotor_rightFront.setPower(v2);
        innoBot.wheelMotor_rightBack.setPower(v4);

        // Show the elapsed game time and wheel power.
        telemetry.addData("Status", "Run Time: " + runtime.toString());
        telemetry.addData("Wheel Motors", "lf (%.2f), rf (%.2f), lb (%.2f), rb (%.2f)", v1, v2, v3, v4);
        telemetry.addData("Gamepad 1 Left/Right Stick", "LX (%.2f), LY (%.2f), RX (%.2f), RY (%.2f)", gamepad1.left_stick_x, gamepad1.left_stick_y, gamepad1.right_stick_x, gamepad1.right_stick_y);
        telemetry.addData("Gamepad 2 Left/Right Stick", "LX (%.2f), LY (%.2f), RX (%.2f), RY (%.2f)", gamepad2.left_stick_x, gamepad2.left_stick_y, gamepad2.right_stick_x, gamepad2.right_stick_y);
        telemetry.addData("Pickup Angle Servo Position", "(%.3f)", pickupAngleServoPosition);
        telemetry.addData("Pickup Motor Power", "(%.3f)", pickupMotorPower);
        telemetry.addData("Transfer Motor Power", "(%.3f)", transferMotorPower);
        telemetry.addData("Lifter Motor Power", "(%.3f)", lifterMotorPower);
        telemetry.addData("Placer CServo Power - Bottom", "(%.3f)", placerCServoPower_bot);
        telemetry.addData("Placer CServo Power - Top", "(%.3f)", placerCServoPower_top);
        telemetry.update();
    }

    /* Code to run ONCE when the driver hits INIT */
    @Override
    public void init() {
        /* Initialize the hardware variables.
         * The init() method of the hardware class in file InnoBot does all the work here
         */
        innoBot.init(hardwareMap);
        telemetry.addData("Good Luck", "2017-2018 Season");
    }

    /* Code to run REPEATEDLY after the driver hits INIT but before they hit PLAY */
    @Override
    public void init_loop() {
    }

    /* Code to run ONCE after the driver hits STOP */
    @Override
    public void stop () {
        innoBot.stopAllMotors();
    }

    /* Code to run ONCE when the driver hits PLAY */
    @Override
    public void start() {
        runtime.reset();
    }
}